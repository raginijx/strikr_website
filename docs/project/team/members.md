
![Strikr Project Team](../../img/tux_linux.png)

##### Team


| IRC NICK          | GIT ID            | EMAIL             | FINGERPRINT                                         | GPG Key           |
|:------------------|:------------------|:------------------|:----------------------------------------------------|:------------------|
|rjx                | raginijx          | ragini@           |2BF62940D2C50FBE10393AAC2A7B7A5A14094746             |[public](https://codeberg.org/raginijx/key/raw/branch/main/key)|
|msk                | saifi             | saifi@            |BF9F4E8F174FCD9C30CD7E61FF3F0F30E1616014             |[public](https://codeberg.org/saifi/key/raw/branch/main/key)|
|akg                | akg               | ajay@             |-                                                    |[public](https://codeberg.org/akg/key/raw/branch/main/key)|



