
![Strikr C++ book](../../img/cc_book.png)

##### ISO C++ keywords

C++ keywords have a predefined meaning in the C++ language and supported by world-class free software compilers such as GCC. Here is the list.

| C++                  | C           |
:----------------------|:------------|
|alignas               |-            |
|alignof               |-            |
|asm                   |-            |
|auto                  |auto         |
|bool                  |-            |
|break                 |-            |
|case                  |case         |
|catch                 |-            |
|char                  |char         |
|char8_t               |-            |
|char16_t              |-            |
|char32_t              |-            |
|class                 |-            |
|concept               |-            |
|const                 |const        |
|consteval             |-            |
|constexpr             |-            |
|constinit             |-            |
|const_cast            |-            |
|continue              |continue     |
|co_await              |-            |
|co_return             |-            |
|co_yield              |-            |
|decltype              |-            |
|default               |default      |
|delete                |-            |
|do                    |do           |
|double                |double       |
|dynamic_cast          |-            |
|else                  |else         |
|enum                  |enum         |
|explicit              |-            |
|export                |-            |
|extern                |extern       |
|false                 |-            |
|float                 |float        |
|for                   |for          |
|friend                |-            |
|goto                  |goto         |
|if                    |if           |
|import                |-            |
|inline                |inline       |
|int                   |int          |
|long                  |long         |
|module                |-            |
|mutable               |-            |
|namespace             |-            |
|new                   |-            |
|noexcept              |-            |
|nullptr               |-            |
|operator              |-            |
|private               |-            |
|protected             |-            |
|public                |-            |
|register              |register     |
|reinterpret_cast      |-            |
|requires              |-            |
|return                |return       |
|short                 |short        |
|signed                |signed       |
|sizeof                |sizeof       |
|static                |static       |
|static_assert         |-            |
|static_cast           |-            |
|struct                |struct       |
|switch                |switch       |
|template              |-            |
|this                  |-            |
|thread_local          |-            |
|throw                 |-            |
|true                  |-            |
|try                   |-            |
|typedef               |typedef      |
|typeid                |-            |
|typename              |-            |
|union                 |union        |
|unsigned              |unsigned     |
|using                 |-            |
|virtual               |-            |
|void                  |void         |
|volatile              |volatile     |
|wchar_t               |-            |
|while                 |while        |



##### C++ Reserved keywords (TS)

* observed in the TS docs
      - atomic_cancel
      - atomic_commit
      - atomic_noexcept
      - reflexpr
      - synchronized
      - transaction_safe
      - transaction_safe_dynamic


##### C++ Reserved identifiers and keywords

* reserved for future use in C++
      - register
      
* alternative representations for certain operators and punctuators in C++
      - and
      - and_eq
      - bitand
      - bitor
      - compl
      - not
      - not_eq
      - or
      - or_eq
      - xor
      - xor_eq

* reserved for use by C++ implementations
      - _Uppercase (each identifier that begins with underscore followed by an Uppercase letter)
      - __identifier (each identifier that begins with or contains a double underscore)

* reserved for use by C++ implementation as a global namespace
      - _identifier (each identifier that begins with an underscore)


##### C++ reserved keywords (GCC)

* keywords reserved for concepts
      - assumes
      - axiom
      - forall
      - concept
      - requires


##### C Reserved identifiers and keywords

* ISO C reserved keywords that should **not** be used in Strikr C++ code base.
      - C99
        - _Bool
        - _Complex
        - _Imaginary
      - C11
        - _Alignas
        - _Alignof
        - _Atomic
        - _Generic
        - _Noreturn
        - _Static_assert
        - _Thread_local
      - C23
        - _Decimal32
        - _Decimal64
        - _Decimal128


##### Strikr reserved words

* list of words that should **not** be used as identifiers in Strikr C++ code base
      - abstract
      - atomic
      - and
      - as
      - assert
      - boolean
      - base
      - begin
      - byte
      - checked
      - component
      - constraint
      - constructor
      - delegate
      - done
      - downcast
      - downto
      - eager
      - elif
      - end
      - event
      - exception
      - extends
      - extern
      - external
      - final
      - finally
      - fixed
      - fn
      - fun
      - functor
      - implements
      - in
      - include
      - inherit
      - instanceof
      - interface
      - internal
      - lazy
      - let
      - match
      - member
      - method
      - mixin
      - mod
      - native
      - non-sealed
      - null
      - object
      - of
      - open
      - override
      - package
      - parallel
      - permits
      - process
      - pure
      - rec
      - record
      - sealed
      - select
      - sig
      - strictfp
      - super
      - synchronized
      - tailcall
      - then
      - throws
      - to
      - trait
      - transient
      - type
      - upcast
      - use
      - val
      - var
      - when
      - with
      - yield


---
Note:

* important changes and clarifications since **C++17**.