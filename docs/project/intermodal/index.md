
![Strikr InterModal Project](../../img/strikr_intermodal.png)

##### Strikr InterModal project

InterModal is an experiment to create a innovation platform based on AMD64 hardware architecture and built out of the Strikr [project baseline](../../engineering/baseline.md)



##### References

* [AMD Secure Encrypted Virtualization](https://developer.amd.com/sev/)
* [Linux Kernel AMD SEV](https://www.kernel.org/doc/html/latest/virt/kvm/amd-memory-encryption.html)
* [AMD SEV White Paper](http://amd-dev.wpengine.netdna-cdn.com/wordpress/media/2013/12/AMD_Memory_Encryption_Whitepaper_v7-Public.pdf)
* [AMD SEV API Spec](https://support.amd.com/TechDocs/55766_SEV-KM_API_Specification.pdf)
* [AMD APM](https://support.amd.com/TechDocs/24593.pdf)
* [AMD Memory Encryption Technology](https://www.linux-kvm.org/images/7/74/02x08A-Thomas_Lendacky-AMDs_Virtualizatoin_Memory_Encryption_Technology.pdf)


