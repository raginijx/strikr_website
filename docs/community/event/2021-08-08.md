
![PERL](../../img/perl.png)

##### PERL Applied Hackathon

- date: 2021-08-08
- time: (UTC+0000) 0800 hrs
- type: Hackathon
- services
    - [#strikr](../irc.md)
    - [foss@strikr.io](../list.md)
    - [meet.jit.si/strikr](../jitsi.md)

##### Problem Statement

re-implement ArchLinux utility scripts using PERL

- Hands-on coding activity on GNU/Linux system
- Focus on modularity, interface and testing
- repo: [arch-install-scripts](https://github.com/archlinux/arch-install-scripts)



<hr />
image credit: [The Perl Foundation](https://www.perlfoundation.org/)