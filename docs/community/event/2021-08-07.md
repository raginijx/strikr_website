
![PERL](../../img/perl.png)

##### Own the shell with PERL

- date: 2021-08-07
- time: (UTC+0000) 0800 hrs
- type: saturday meet

##### Overview

PERL is a general-purpose paradigm-free programming language with a very expressive syntax.

The purpose of the session is to learn about this amazing language and how it integrates so well with the GNU/Linux for a wide-variety of tasks which are performed by sh,sed,awk,regex,expect,tcl among others.

You are expected to be running a GNU/Linux or POSIX compliant environment for working on the code.

##### Further Reading

- [Purpose of PERL](https://www.quora.com/What-is-the-Perl-programming-language-purpose?share=1)
- [perldoc](https://perldoc.perl.org/)
- [cpan](https://www.cpan.org/)
- [perl.com](https://www.perl.com/)



##### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.


<hr />
image credit: [The Perl Foundation](https://www.perlfoundation.org/)
