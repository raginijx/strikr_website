
![With All Due Respect](../img/wadr.png)

##### WADR

WADR or (W)ith (A)ll (D)ue (R)espect is a cheeky fun-filled technical event.

the objective of WADR would be

- deconstruct other technologies, projects from a hacker / deeper perspective.
- understand the core idea
- explore the implementation
- study their bug reports / issues very closely
- learn from their architectural limitations, mistakes

Then we put on our own thinking caps and come up with our own design. Given sufficient interest we may even go ahead and do our own implementation.

Finally as to the usage of the phrase WADR, we read an apt description (@st_vincent) of it while doing an internet search.

"with all due respect" is a wonderful expression because it doesn't actually specify how much respect is actually due. *Could be none*.

If you like a detailed story, please read the [original post](https://www.mail-archive.com/foss@strikr.io/msg00525.html).


##### Frequency

- as and when there is demand for respect ;-)

##### Timings

- Global (UTC+0000) 0800 - 1200 hrs
- City specific [timings](timings.md)


##### Events

Please checkout the [list of upcoming events](event/index.md)


#### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.


