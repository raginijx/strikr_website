Terms that commonly show up as part of our community conversations.

#####   A

- ASS Always Speak Sense

#####  B 

- BTL Be The Leader
- BRO BRutal hOnesty

#####  C 

- CAB Colony and Bazaar
- CBT Consistency Beats Talent
- CIS Cultural & Institutional Support
- CTN Change The Narrative

#####  D 

#####  E 

#####  F 

- FTC From Their Colony

#####  G

- GDP Geographic Diversity of Participants

#####  H

#####  I

#####  J

#####  K

#####  L

#####  M

 - MAFANGO

#####  N

#####  O

#####  P

- PAF Patience and Focus
- PFP People from Past
- POS Position of Strength
- PSX Positive Support Experience

#####  Q

#####  R

- RAT Radical Transparency
- RCS Random Cool Stack
- RTS Ready To Ship

#####  S

- SMS Something More Significant
- SRM Stable Reliable Maintenable

#####  T

- TAR Types Algorithms and Relationships
- TFI Talent For Innovation
- TFM Talent For Imitation
- TWT Time Wasting Tactic

#####  U

- UDA Up Down Across

#####  V

#####  W

- WADR With All Due Respect

#####  X

- XAC 'X' as Code

#####  Y

- YFC Your First Contribution

#####  Z


