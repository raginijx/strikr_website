
##### IRC

IRC or [Internet Relay Chat](https://en.wikipedia.org/wiki/Internet_Relay_Chat) is a application layer protocol that facilitates text-based communication. It is a real-time light-weight and scalable form of communication.


We are thankful to the [HackInt](https://hackint.org/) community for providing IRC services to the free software community.

![IRC HackInt](../img/hackint.png)

We are thankful to the [Libera.chat](https://libera.chat/about) folks for supporting the free software community.

![IRC Libera Chat](../img/libera.png)


At Strikr, we make extensive use of IRC as part of our community interaction and co-ordination of activities.



##### IRC Channel

our official registered IRC channel is

- \#strikr IRC channel on HackInt
- \#strikr IRC channel on Libera.chat



##### Webchat

Both the IRC services provide a web interface for IRC chat.

- HackInt      [https://webirc.hackint.org/](https://webirc.hackint.org/)
- Libera.chat  [https://web.libera.chat/](https://web.libera.chat/)



##### IRC client we use

We recommend using the following IRC clients directly on the console or linux terminal

* **Irssi**
  irssi is a highly configurable IRC client with text mode interface.
  The user contributed PERL scripts are maintained at [https://scripts.irssi.org](https://scripts.irssi.org)
* **Emacs**
  Emacs RCIRC is written in Emacs LISP.
  Learn more at [https://www.emacswiki.org/emacs/rcirc](https://www.emacswiki.org/emacs/rcirc)



##### Nick Registration

You need to register your 'nick' using 'NickServ' IRC service to effectively and safely use the IRC.



##### Help

Need help with configuration ? Have queries ? Please write to community mailing list [foss@strikr.io](mailto:foss@strikr.io)
