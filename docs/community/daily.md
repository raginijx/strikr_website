
![Strikr DCH](../img/tux_office.png)


##### Daily Community Hours

As part of Strikr governance model, we came up with the idea of organizing daily community hours aka DCH using internet relay chat.

DCH happen on **\#strikr** [IRC](irc.md) channel.

Anyone can initiate a discussion or seek assistance or share details of their exploration. Please remember to keep your discussion relevant in the context of free software and strikr baseline.

Please join the DCH and network with the community members.


##### Days

Su - Mo - Tu - We - Th - Fr


##### Timings

* Global (UTC+0000) 0800 - 0930 hrs
* City specific [timings](./timings.md)


##### Connect

Connect with the community members on **\#strikr** [IRC](irc.md) channel.


##### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.
