
![GNU GCC Compiler](../img/cc_gnu_gcc.png)

#### GNU GCC C++ compiler

The [GNU Compiler Collection](https://gcc.gnu.org/) aka GCC is an optimizing compiler produced by the [GNU Project](https://gnu.org) with excellent support for modern [ISO C++ 20](https://ref.strikr.io/libstdcxx/lib/manual/status.html#status.iso.2020) and upcoming versions. GCC supports C++ on various hardware architectures and operating systems.

GCC is a key component of the GNU toolchain and the standard compiler for most projects related to GNU and the Linux kernel.


##### GCC Overview

Please listen to the interview with Morgan Deters about GCC which covers all steps from the parsing of different programming languages to machine independenet optimizations and generating processor specific binary code.

<iframe width="560" height="315" src="https://www.youtube.com/embed/OYDocHc8sqc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##### Reference

- [GNU Compiler Collection](https://en.wikipedia.org/wiki/GNU_Compiler_Collection)
- [GNU Toolchain](https://en.wikipedia.org/wiki/GNU_toolchain)



<hr />
image credit: FSF, GNU, GCC project.