
![GNU Emacs](../img/gnu_emacs.png)

GNU Emacs is the editor of choice for our work at Strikr.

Here are Strikr Notes for productive Emacs usage.

---

man page

* M-x man

---

Wrap

* M-x toggle-truncate-lines

---

Font

* M-x font-lock-mode
* M-x font-lock-comment-face "black"
* M-x customize-face

---

Search

* C-s
* C-s C-w ... C-s

---

M-x cua-mode

* C-c
* C-v
* C-z

---

Execution

* M-x compile
* M-x recompile
* M-x executable-interpret

---

Window

* C-x 0   delete current window
* C-x 1   delete other window
* C-x 2
* C-x 3   split window horizontally
* C-x +   balance window

----

Buffer

* M-x buffer-menu
* M-x kill-this-buffer
* C-x k
* M-x revert-buffer
* C-x -> buffer next
* C-x <- buffer prev

---

Cursor movement

* M-{
* M-}
* M-<
* M->
* M-C-f  match opening '{'
* M-C-b  match closing '}'

---

File

* M-x find-file-at-point
* M-x find-name-dired

---

change r,w,x mode for a file

* M-x set-file-modes <RET> file_name <RET> u+x
* M-x set-file-modes <RET> file_name <RET> 0700
* M-x chmod <RET> file_name <RET> u+x

---

Tags read TAGS file

* M-x visit-tags-table

Tags navigation

* M-x xref-find-apropos
* M-x xref-find-definitions
* M-.
* M-x xref-pop-marker-stack
* M-,
* M-x xref-find-references
* M-?

---

Colors

List the colors emacs can display

* M-x list-colors-display

Change the font color of comments

* M-x customize-face

Add the following line to ~/.emacs.d/init.el

* (set-face-foreground 'font-lock-comment-face "black")

---

list all the files open (buffer)

* M-x buffer-menu
* C-x C-b

---

<hr />
image credit: Nicolas Petton, copyright FSF, licensed under GPLv3+ from [Emacs git repo](https://git.savannah.gnu.org/cgit/emacs.git/tree/etc/images/icons).
